### Stuff to install
- arandr
- lxappearance
- feh
- at-spi2-core
- yad
- xorg
- firefox
- ssh-askpass
- tmux
- weechat

### Stuff to package
- [obamenu](http://rmoe.anukis.de/obamenu.html)
- tildelinux gtk theme (in this repo)
- (once theme is installed also change default font to monospace)
- tildemerge

### Stuff to customize (package a different version)
- terminator (with config in this repo) - done - in skel repo
- compton (with config in this repo) - done - in skel repo
- openbox (with tilde theme) - done (still todo tilde theme) - in openbox repo
- xfce4-panel (with customization in screenshot)
- lightdm (with tilde theme)
- xfce4-whiskermenu-plugin (with customization in screenshot)
