#!/bin/sh

config_dir="$HOME/.config"
theme_dir="$HOME/.themes"
art_dir="$HOME/.art"

install_folder(){
	origin_dir="$1"
	install_dir="$2"

	test -d "$install_dir" \
	|| mkdir "$install_dir"
	
	find "./$origin_dir" -type f | while read -r file
	do
		dest_path="$(echo "$install_dir/$file" | sed 's/\/\.\/'"$origin_dir"'//')"
		dest_dir="$(dirname "$dest_path")"
	
		test -d "$dest_dir" \
		|| mkdir -p "$dest_dir"
	
		if cp -f "$file" "$dest_path"
		then
			sed -ie "s/{{{ USER }}}/$USER/g" "$dest_path" >/dev/null 2>&1
			echo "Installed $file to $dest_path"
			echo "$dest_path" >> "$config_dir/.installed_by_tildelinux"
		fi
	done
}

echo "~~ Configs ~~"
install_folder dotconfig "$config_dir"
echo "~~ Themes~~"
install_folder themes "$theme_dir"
echo "~~ Art ~~"
install_folder art "$art_dir"
