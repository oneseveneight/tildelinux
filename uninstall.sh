#!/bin/sh

config_dir="$HOME/.config"

while read -r file
do
	rm "$file"
	echo "Removed $file"
done < "$config_dir/.installed_by_tildelinux"
